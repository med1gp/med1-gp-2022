## 作品制作

### 参加条件はありますか？

ありません。ニコニコメドレーを制作できる方であればどなたでも参加可能です。

### 匿名参加について（注：「名義を隠しての参加」「偽名での参加」と捉えてお答えしています）

連絡先（Twitter,Discord,メールアドレスのいずれか）がわかれば OK です。但し、1 人による複数名義での参加はご遠慮ください。

### 公開されている MIDI は使用可能ですか？

インターネット上に公開されている MIDI であれば（その MIDI の使用規約に抵触しない限りは）使用可能です。但し、本大会のために他者に耳コピ等を依頼することは禁止とします。

### 映像無しでも大丈夫ですか？

ファイル形式が mp4 であれば背景画像のみの映像の動画の提出でも何ら問題ありません。
但し、音声ファイルのみで提出した場合の参加は認められません。

### 映像に削除素材を使用することは可能ですか？

禁止事項の「ニコニコ生放送で扱う上で不適切なもの」に該当する可能性があるものは運営から修正をお願いします。（少しでも懸念点がある場合は提出時に備考欄に記載していただけると助かります）

### メドレーの一部を提出し、後日フルを公開してもよいか

レギュレーション上では可能ですが、あくまで Med-1 グランプリ 2022 において審査対象となるのは提出された部分のみとなります。

### 告知はダメですか？

提出する動画に関わる音声・映像・画像は禁止事項の「本放送前の公の場での公開」に抵触し失格処分となるためできません。
事前に公開できる作品情報の項目を特設サイトに掲載いたしますので、告知前にご確認ください。

### 「メドレーの一部を提出し、後日フルを公開してもよいか」という質問がありましたが、提出する部分が未公開ならばフルにする時追加する部分が公開されていても大丈夫なのでしょうか？

（円滑に本企画を進めるという良識の範囲内であれば）OK です。
勿論「審査対象はあくまで提出部分のみ」かつ「事前に公開された作品を視聴するかどうかは審査員に一任する」という形をとっておりますので、その点のみご了承ください。

### 既に公開しているメドレー（183 含む）で使用した構成を一部流用することは事前の公開のレギュレーションに抵触するでしょうか？

（円滑に本企画を進めるという良識の範囲内であれば）OK です。
理由 ①：作業が短縮できるというメリットがある反面、既に公開済みであるため新鮮味に薄れるというデメリットもあるため。
理由 ②：「過去のニコニコメドレーをリスペクトする」という手法は既に多くのニコニコメドレー作品に取り入れられており、違反とする根拠としては薄いため。

## 本放送

### 当日流れるニコニコメドレーの順番について

スケジュールが確定し次第改めてお伝えしますが、くじ引きで順番を決定する生放送を行う予定です。（予定は予告なく変更される場合があります）

### 審査基準を教えてください

審査基準、ならびに審査について審査員の皆様にお伝えしている文面は非公開となります。
皆様が制作されるニコニコメドレーに影響を及ぼさないことが目的です。

### 得点発表のタイミングについて

各作品が流れ終わったタイミング毎に採点し、その都度得点を発表します。

### 決勝戦はありますか？

いわゆる〝上位 3 組による最終決戦〟はありません。審査で発表された得点の順番がそのまま順位となります。

### 景品はありますか？

鋭意検討中です。可能であれば用意する方向で考えております。
