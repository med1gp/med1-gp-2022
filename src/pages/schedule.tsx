import { InferGetStaticPropsType } from "next"
import React from "react"

import Layout from "@/components/layouts/subLayout"
import MainSchedule from "@/components/schedule/MainSchedule"
import TimeTableList from "@/components/schedule/TimeTableList"
import SEO from "@/components/seo"
import { getSchedule, getTimeTable } from "@/services/schedule"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = async () => {
  const schedules = getSchedule()
  const timeTable = getTimeTable()

  return {
    props: {
      schedules,
      timeTable
    }
  }
}

const SchedulePage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ schedules, timeTable }) => {
  return (
    <>
      <SEO title="日程" />
      <div className="my-16 px-4">
        <h5 className="text-5xl font-bold">全体スケジュール</h5>
        <MainSchedule schedules={schedules} />
      </div>
      <div className="my-16 px-4">
        <div className="mb-8">
          <h5 className="mb-4 text-5xl font-bold">本放送タイムテーブル</h5>
          <p className="text-lg">※ 時刻は予定です。放送の進行の都合上変更になる場合がございます。</p>
        </div>
        <TimeTableList timeTable={timeTable} />
      </div>
      <div className="my-16 px-4">
        <div className="mb-8">
          <h5 className="mb-4 text-5xl font-bold">アーカイブ</h5>
        </div>
        <iframe width="312" height="176" src="https://ext.nicovideo.jp/thumb_series/320687"><a href="https://www.nicovideo.jp/series/320687">Med-1グランプリ2022 アーカイブ</a></iframe>
      </div>
    </>
  )
}

SchedulePage.getLayout = page => <Layout pageTitle={{ ja: "日程・タイムテーブル", en: "time table" }}>{page}</Layout>

export default SchedulePage
