import '@/styles/index.css'

import { NextPage } from "next";
import { AppProps } from "next/app";
import { ReactElement } from "react";

import SEO from "@/components/seo";

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactElement
}

type AppPropsWithLayout<P = {}> = AppProps<P> & {
  Component: NextPageWithLayout<P>
}

export default function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? (page => page)

  return <>
    <SEO description="目指せ、ニコニコメドレーの頂点。Med-1 GP 2022！" />
    {getLayout(
      <Component {...pageProps} />
    )}
  </>
}