import { NextPage } from "next"
import { useRouter } from "next/router"
import React, { useEffect } from "react"

const NotFoundPage: NextPage = () => {
  const router = useRouter()

  useEffect(() => {
    router.replace('/')
  }, [])

  return <div />
}

export default NotFoundPage
