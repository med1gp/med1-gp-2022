import { InferGetStaticPropsType } from "next"
import React from "react"

import DefaultLayout from "@/components/layouts/defaultLayout"
import MainArchive from "@/components/main/MainArchive"
import MainBanner from "@/components/main/MainBanner"
import MainInformation from "@/components/main/MainInformation"
import MainShare from "@/components/main/MainShare"
import MainSocial from "@/components/main/MainSocial"
import { getWinnerWork } from "@/services/worklist"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = async () => {
  const winner = await getWinnerWork()

  return {
    props: {
      winner
    }
  }
}

const IndexPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ winner }) => (
  <>
    <MainBanner winner={winner} />
    <MainShare />
    <MainInformation />
    <MainArchive />
    <MainSocial />
  </>
)

IndexPage.getLayout = page => <DefaultLayout>{page}</DefaultLayout>

export default IndexPage
