import { InferGetStaticPropsType } from "next"
import React from "react"
import { FaPlay, FaStop } from 'react-icons/fa'

import Layout from "@/components/layouts/subLayout"
import SEO from "@/components/seo"
import { getEdInfo } from "@/services/ed"
import { useBreakpoint } from "@/utils/useBreakpoint"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = () => {
  return {
    props: getEdInfo()
  }
}

const volumeMax = 0.25

let audio: null | HTMLAudioElement


const musicPlay = (url: string, index: number, playNum: number, playEnd: boolean) => {
  audio!.pause()
  if (playNum === index + 1) {
    return 0
  }
  audio!.src = url
  audio!.volume = 0
  console.log()
  audio!.currentTime = (playEnd) ? 170 : 0
  audio!.autoplay = true
  return index + 1
}

const musicStop = () => {
  audio!.pause()
}

const fadeIn = (e: Event) => {
  const timer = setInterval(() => {
    audio!.volume += (volumeMax / 100)
    if (audio!.volume >= volumeMax) clearInterval(timer)
  }, 10)
}

const EdPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = (ed) => {
  const { md } = useBreakpoint()
  const [playNum, setPlayNum] = React.useState(0)
  const [playEnd, setPlayEnd] = React.useState(true)
  React.useEffect(() => {
    audio = document.querySelector("audio")

    audio!.addEventListener('play', fadeIn)
    window.addEventListener('beforeunload', musicStop)
  })
  return (
    <>
      <SEO title="エンディング" />
      <div className="my-16 px-4">
        <div className="max-w-9xl mx-auto w-full bg-base-300 p-8 md:w-9/12 xl:w-6/12">
          <div className="mb-8 flex justify-center">
            <iframe
              src="https://embed.nicovideo.jp/watch/sm40425496"
              className="min-h-max w-full"
              width="640"
              height="360"
            />
          </div>
          <a className="btn-primary btn-block btn" href={ed.downloadUrl} target="_blank" rel="noreferrer">ダウンロード</a>
        </div>
        <div className="relative mt-16 mb-8 align-text-bottom">
          <h5 className={(md) ? "absolute text-5xl font-bold" : "absolute bottom-0 text-xl font-bold"}>トラックリスト</h5>
          <div className="right-0 flex justify-end">
            <div className="form-control">
              <label className="label cursor-pointer bg-base-200 px-2">
                <span className="label-text mx-4">最後から再生する</span>
                <input type="checkbox" onChange={(e) => { setPlayEnd(e.target.checked) }} defaultChecked className="toggle-primary toggle toggle-lg"></input>
              </label>
            </div>
          </div>
        </div>
        <table className="table-zebra table w-full ring-1 ring-neutral-900">
          <thead>
            <tr>
              <th>#</th>
              <th></th>
              <th className="text-center">試聴</th>
            </tr>
          </thead>
          <tbody>
            {ed.songList.map((value, index) => (
              <tr className={(playNum === index + 1) ? "border border-primary-focus" : ""} key={index}>
                <th>{index + 1}</th>
                <td>
                  <div className="flex flex-row">
                    {
                      (md) ?
                        (<div className="mr-8 self-center text-primary">Med-1GP 2022 エンディング Ver.</div>) :
                        (<div className="mr-2 self-center text-sm text-primary">Ver.</div>)
                    }
                    <div>
                      <div className={(md) ? "text-3xl font-bold" : "font-bold"}>
                        {value.songName}</div>
                      {(!value.specialThanks) ? null :
                        (<div className={`badge-secondary badge-outline badge ${(md) ? "" : "badge-sm"}`}>協力 / {value.specialThanks}</div>)}
                    </div>
                  </div>
                </td>
                <td className="flex justify-center">
                  <button className={`btn-ghost btn ${(md) ? "btn-lg -my-1" : ""} btn-primary btn-circle justify-self-center`} onClick={() => {
                    setPlayNum(musicPlay(value.songUrl, index, playNum, playEnd))
                  }}>{(playNum === index + 1) ? (<FaStop></FaStop>) : (<FaPlay></FaPlay>)}</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <audio id="audio" onEnded={() => { setPlayNum(0) }}></audio>
      </div>
    </>
  )
}

EdPage.getLayout = page => <Layout pageTitle={{ ja: "エンディング", en: "ending" }}>{page}</Layout>

export default EdPage
