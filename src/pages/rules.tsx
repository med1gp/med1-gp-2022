import { InferGetStaticPropsType } from "next"
import React from "react"
import { ReactMarkdown } from "react-markdown/lib/react-markdown"
import rehypeRaw from "rehype-raw"

import Layout from "@/components/layouts/subLayout"
import SEO from "@/components/seo"
import { getMarkdownData } from "@/services/markdown"
import styles from "@/styles/rules.module.css"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = async () => {
  const data = await getMarkdownData('rules')

  return {
    props: {
      data
    }
  }
}

const RulesPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ data }) => (
  <>
    <SEO title="レギュレーション" />
    <div className="px-4 py-10">
      <ReactMarkdown
        className={styles.rules}
        rehypePlugins={[rehypeRaw]}
      >
        {data}
      </ReactMarkdown>
    </div>
  </>
)

RulesPage.getLayout = page => <Layout pageTitle={{ ja: "レギュレーション", en: "regulation" }}>{page}</Layout>

export default RulesPage
