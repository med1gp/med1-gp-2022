import { InferGetStaticPropsType } from "next"
import Link from "next/link"
import React from "react"
import { ReactMarkdown } from "react-markdown/lib/react-markdown"
import { Link as ScrollLink } from "react-scroll"
import rehypeSlug from 'rehype-slug'

import Layout from "@/components/layouts/subLayout"
import SEO from "@/components/seo"
import { getMarkdownData } from "@/services/markdown"
import styles from "@/styles/qna.module.css"

import { NextPageWithLayout } from "./_app"


export const getStaticProps = async () => {
  const data = await getMarkdownData('qna')

  return {
    props: {
      data
    }
  }
}

const QnaPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ data }) => {
  const qnaCategory = ["作品制作", "本放送"]

  return (
    <>
      <SEO title='よくある質問' />
      <div id="wrapper">
        <div className="grid grid-cols-1 md:grid-cols-3 2xl:grid-cols-4">
          <div className="mt-8 p-4">
            <div className="top-20 flex  md:sticky md:flex-col">
              {qnaCategory.map((it, idx) => (
                <ScrollLink
                  to={it}
                  key={`qna-${idx}`}
                  smooth={true}
                  offset={-100}
                  containerId="drawer-content"
                  className="btn mr-3 mb-0 bg-white/20 md:mr-0 md:mb-3"
                >
                  {it}
                </ScrollLink>
              ))}
              <Link
                href="https://marshmallow-qa.com/med1gp"

                passHref
              >
                <a className="btn bg-marshmallow text-black" rel="noreferrer" target="_blank">
                  マシュマロで質問する
                </a>
              </Link>
            </div>
          </div>
          <div className="col-span-2 my-8 px-4 2xl:col-span-3">
            <ReactMarkdown
              className={styles.qna}
              rehypePlugins={[rehypeSlug]}
            >
              {data}
            </ReactMarkdown>
          </div>
        </div>
      </div>
    </>
  )
}

QnaPage.getLayout = page => <Layout pageTitle={{ ja: "よくある質問", en: "q&a" }}>{page}</Layout>

export default QnaPage
