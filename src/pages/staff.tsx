import { InferGetStaticPropsType } from "next"
import React from "react"

import Layout from "@/components/layouts/subLayout"
import SEO from "@/components/seo"
import Med1StaffCard from "@/components/staff/Med1StaffCard"
import { getStaffInfo } from "@/services/staff"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = () => {
  const { host, panel, staff } = getStaffInfo()

  return {
    props: {
      host, panel, staff
    }
  }
}

const StaffPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ host, panel, staff }) => {
  return (
    <>
      <SEO title="スタッフ" />
      <div className="px-4 py-10">
        <div className="mb-4 rounded-lg bg-gray-900 p-4">
          <h4 className="text-xl">主催・運営</h4>
        </div>
        <div className="mb-4 grid gap-6 md:grid-cols-2 xl:grid-cols-3">
          {host.map((it, idx) => (
            <Med1StaffCard staff={it} key={`host-${idx}`} />
          ))}
        </div>
        <div className="mb-4 rounded-lg bg-gray-900 p-4">
          <h4 className="text-xl">審査員</h4>
        </div>
        <div className="mb-4 grid gap-6 md:grid-cols-2 xl:grid-cols-3">
          {panel.map((it, idx) => (
            <Med1StaffCard staff={it} key={`panel-${idx}`} />
          ))}
        </div>
        <div className="mb-4 rounded-lg bg-gray-900 p-4">
          <h4 className="text-xl">制作スタッフ</h4>
        </div>
        <div className="mb-4 grid gap-6 md:grid-cols-2 xl:grid-cols-3">
          {staff.map((it, idx) => (
            <Med1StaffCard staff={it} key={`staff-${idx}`} />
          ))}
        </div>
      </div>
    </>
  )
}

StaffPage.getLayout = page => <Layout pageTitle={{ ja: "スタッフ", en: "staff" }}>{page}</Layout>

export default StaffPage
