import { InferGetStaticPropsType } from "next"
import React from "react"

import Layout from "@/components/layouts/subLayout"
import MobileResultTable from "@/components/results/MobileResultTable"
import PcResultTable from "@/components/results/PcResultTable"
import SEO from "@/components/seo"
import { getResultData } from "@/services/worklist"
import { useBreakpoint } from "@/utils/useBreakpoint"

import { NextPageWithLayout } from "./_app"


export const getStaticProps = async () => {
  const list = getResultData()

  return {
    props: {
      list,
    }
  }
}

const ResultPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> = ({ list }) => {
  const { md } = useBreakpoint()

  return (
    <>
      <SEO title='結果' />
      <div className="my-16 w-full">
        {md ? <PcResultTable list={list} /> : <MobileResultTable list={list} />}
      </div>
    </>
  )
}

ResultPage.getLayout = page => <Layout pageTitle={{ ja: "結果", en: "result" }}>{page}</Layout>

export default ResultPage
