import { InferGetStaticPropsType } from "next"
import React, { ChangeEventHandler, useState } from "react"

import Layout from "@/components/layouts/subLayout"
import SEO from "@/components/seo"
import {
  WorkListByEntry,
  WorkListByNatural,
} from "@/components/works/Med1WorkList"
import { getWorkList } from "@/services/worklist"

import { NextPageWithLayout } from "./_app"

export const getStaticProps = () => {
  const { worksList, works1days, works2days } = getWorkList()

  return {
    props: {
      worksList,
      works1days,
      works2days
    }
  }
}

type SortType = 'Natural' | 'Entry'

const WorksPage: NextPageWithLayout<InferGetStaticPropsType<typeof getStaticProps>> =
  ({
    worksList,
    works1days,
    works2days
  }) => {

    const [sort, setSort] = useState<SortType>('Natural')
    const onSortChanged: ChangeEventHandler<HTMLSelectElement> = (e) => setSort(e.target.value as SortType)

    const SortController = () => (
      <>
        <label className="label">
          <span className="label-text-alt">並び替え順</span>
        </label>
        <select
          className="select-bordered select w-full max-w-xs"
          onChange={onSortChanged}
          value={sort}
        >
          <option value='Natural'>出場順</option>
          <option value='Entry'>エントリーナンバー順</option>
        </select>
      </>
    )


    return (
      <>
        <SEO title="作品リスト" />
        <div className="px-4">
          <div className="my-10">
            <SortController />
          </div>
          <div className="mb-32">
            {sort === 'Natural' && <WorkListByNatural first={works1days} second={works2days} />}
            {sort === 'Entry' && <WorkListByEntry list={worksList} />}
          </div>
        </div>
      </>
    )
  }

WorksPage.getLayout = page => <Layout pageTitle={{ ja: "参加者・参加作品", en: "entry & works" }}>{page}</Layout>

export default WorksPage
