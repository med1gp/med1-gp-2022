
//  Clipboard APIでのページ情報のコピペ
type RunCopy = (title: string, descripion: string, url: string) => (boolean | Promise<boolean>)

export const runCopy: RunCopy = async ( title, description, url ) => {
  const data = `${title}\n${description}\n${url}`

  if (!window?.navigator?.clipboard?.writeText) {
    return Promise.resolve(false)
  }

  return window?.navigator?.clipboard?.writeText?.(data).then(() => true).catch(() => false)
}

type RunShare = (title: string, descripion: string, url: string) => Awaited<void>

//  Web Share APIでの共有メニュー呼び出し
export const runShare: RunShare = ( title, description, url ) => {
  const shareData = {
    title,
    text: description,
    url,
  }

  //  エラー時にはコピペを実行
  window?.navigator?.share?.(shareData)?.catch?.(() => { })
}
