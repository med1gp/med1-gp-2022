import breakpoint from "use-breakpoint"

const BREAKPOINTS = {
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  "2xl": 1536,
}

export const useBreakpoint = () => {
  const { breakpoint: bp } = breakpoint(BREAKPOINTS, "sm")

  return {
    sm: !["sm"].includes(bp),
    md: !["sm", "md"].includes(bp),
    lg: !["sm", "md", "lg"].includes(bp),
    xl: !["sm", "md", "lg", "xl"].includes(bp),
    "2xl": !["sm", "md", "lg", "xl", "2xl"].includes(bp),
  }
}
