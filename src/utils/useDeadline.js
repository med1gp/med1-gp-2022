import dayjs from "dayjs"

import schedules from "@/assets/json/schedules.json"

export const useDeadline = () => {
  const [_1, _2, shimekiri] = schedules
  const isOverShimekiri = dayjs(shimekiri.date).add(1, "day").isBefore(dayjs())
  const isPast = date =>
    !!dayjs(date).isValid() && !dayjs().isBefore(dayjs(date), "day")

  return {
    shimekiri,
    isPast,
    isOverShimekiri,
  }
}
