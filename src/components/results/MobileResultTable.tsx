import { Disclosure } from "@headlessui/react"
import clsx from "clsx"
import Link from "next/link"
import { FC, Fragment } from "react"
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai'
import { BiCommentDots } from "react-icons/bi"

import { panelList, ResultWork } from "@/services/worklist"

const getCellBgClass = (work: ResultWork, otherClass = "bg-base-300") => ({
  "bg-gold": work.rank === 1,
  "bg-silver": work.rank === 2,
  "bg-brozen": work.rank === 3,
  [otherClass]: work.rank > 3,
})

const getCellTextClass = (work: ResultWork, otherClass = "text-white") => ({
  "text-white": work.rank <= 3,
  [otherClass]: work.rank > 3,
})

type Props = {
  list: ResultWork[]
}

const MobileCollapse: FC<{ work: ResultWork }> = ({ work }) => {
  return (
    <div className="relative flex flex-col ">
      <Disclosure>
        {({ open }) => (
          <>
            <Disclosure.Button className="flex items-center justify-between bg-black/20 px-4 py-6">
              <span className="opacity-0">
                <AiFillCaretUp />
              </span>
              <span className="text-sm text-white">審査結果表示</span>
              {open ? <AiFillCaretUp /> : <AiFillCaretDown />}
            </Disclosure.Button>
            <Disclosure.Panel className="stat stats-vertical grow bg-black/10">
              {Object.values(work.point).map((point, idx) => (
                <div
                  className="stat place-items-center gap-2 px-0"
                  key={`panel-mobile-${idx}`}
                >
                  <div className="stat-title rounded-full bg-black/10 px-2 py-1 font-bold text-white">
                    {panelList[idx]}
                  </div>
                  <div className="stat-value">{point.value}</div>
                  {point.comment && (
                    <div className="mt-4 rounded bg-black/20 p-4">
                      <h6 className="mb-2 flex items-center gap-1 text-sm font-bold text-white/50">
                        <BiCommentDots />
                        <span> COMMENTS </span>
                      </h6>
                      <p className="whitespace-pre-line break-all text-justify leading-5">
                        {point.comment.body}
                      </p>
                    </div>
                  )}
                </div>
              ))}
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </div>
  )
}

const MobileResultTable: FC<Props> = ({ list }) => (
  <div>
    <div className="grid w-full grid-cols-[1fr_2fr_1fr] bg-base-300 text-center">
      <span className="py-3 text-xs font-bold">順位</span>
      <span className="grow py-3 text-xs font-bold">タイトル</span>
      <span className="py-3 text-xs font-bold">総合得点</span>
    </div>
    <dl>
      {list.map((work, idx) => (
        <Fragment key={`work-${work.entryNo}`}>
          <dt>
            <div className="grid w-full grid-cols-[1fr_2fr_1fr]">
              <div
                className={clsx(
                  `flex items-center justify-center self-stretch py-3`,
                  getCellBgClass(work)
                )}
              >
                <span
                  className={clsx(
                    `font-rankPoint text-3xl font-bold `,
                    getCellTextClass(work, "text-primary")
                  )}
                >
                  {work.rank}
                </span>
              </div>
              <div
                className={clsx(
                  `grow py-3 text-center`,
                  getCellBgClass(work)
                )}
              >
                <Link
                  href={work.video}
                  target="_blank"
                  rel="noreferrer"
                  passHref
                >
                  <a
                    className={clsx(`mb-2 block text-xl font-bold text-white`, {
                      underline: !!work.video,
                    })}

                  >
                    {work.title}
                  </a>
                </Link>
                <span className="text-xs tracking-tighter opacity-80">
                  {work.author}
                </span>
              </div>
              <div
                className={clsx(
                  `flex items-center justify-center self-stretch py-3`,
                  getCellBgClass(work)
                )}
              >
                <span
                  className={clsx(
                    `font-rankPoint text-3xl font-bold`,
                    getCellTextClass(work, "text-primary")
                  )}
                >
                  {work.totalPoint}
                </span>
              </div>
            </div>
          </dt>
          <dd className={clsx(getCellBgClass(work))}>
            <MobileCollapse work={work} />
          </dd>
        </Fragment>
      ))}
    </dl>
  </div>
)

export default MobileResultTable