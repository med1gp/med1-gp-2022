import { Popover, Transition } from "@headlessui/react"
import clsx from "clsx"
import Image from "next/image"
import Link from "next/link"
import { FC, Fragment } from "react"
import { BiCommentDots } from "react-icons/bi"
import { FaPlay } from 'react-icons/fa'
import { IoMdClose } from "react-icons/io"

import { panelList, ResultWork } from "@/services/worklist"

const getCellBgClass = (work: ResultWork, otherClass = "bg-base-300") => ({
  "bg-gold": work.rank === 1,
  "bg-silver": work.rank === 2,
  "bg-brozen": work.rank === 3,
  [otherClass]: work.rank > 3,
})

const getCellTextClass = (work: ResultWork, otherClass = "text-white") => ({
  "text-white": work.rank <= 3,
  [otherClass]: work.rank > 3,
})

type Props = {
  list: ResultWork[]
}

const PcResultTable: FC<Props> = ({ list }) => {
  return (
    <table className="box-border table w-full text-center">
      <colgroup>
        <col />
        <col />
        <col />
        <col width="7.5%" />
        <col width="7.5%" />
        <col width="7.5%" />
        <col width="7.5%" />
        <col width="7.5%" />
        <col width="7.5%" />
        <col />
      </colgroup>
      <thead>
        <tr>
          <th>順位</th>
          <th>タイトル</th>
          <th>総合得点</th>
          {panelList.map(panel => (
            <th key={`panel-header-${panel}`}>{panel}</th>
          ))}
          <th>動画</th>
        </tr>
      </thead>
      <tbody>
        {list.map(work => (
          <tr key={`work-${work.author}`}>
            <td className={clsx(getCellBgClass(work))}>
              <span
                className={clsx(
                  `font-rankPoint text-3xl font-bold`,
                  getCellTextClass(work, "text-primary")
                )}
              >
                {work.rank}
              </span>
            </td>
            <td className={clsx(getCellBgClass(work))}>
              <p
                className={clsx(
                  `mb-2 text-2xl font-bold text-white`,
                )}
              >
                {work.title}
              </p>
              <span className="badge bg-black opacity-80">{work.author}</span>
            </td>
            <td className={clsx(getCellBgClass(work))}>
              <span
                className={clsx(
                  `font-rankPoint text-3xl font-bold`,
                  getCellTextClass(work, "text-primary")
                )}
              >
                {work.totalPoint}
              </span>
              <span className="text-sm">点</span>
            </td>
            {Object.values(work.point).map((point, idx) => (
              <td
                className={clsx('', getCellBgClass(work, "bg-primary"))}
                key={`panel-pc-${idx}`}
              >

                <div className="flex items-center justify-center">
                  <span className="font-rankPoint text-2xl font-bold text-white">
                    {point.value}
                  </span>

                  {
                    point.comment.body && (
                      <Popover className='relative ml-4'>
                        <Popover.Button className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded bg-black/25 px-1 py-0.5 text-sm text-white'>
                          <BiCommentDots />
                        </Popover.Button>
                        <Transition
                          as={Fragment}
                          enter="transition ease-out duration-200"
                          enterFrom="opacity-0 translate-y-1"
                          enterTo="opacity-100 translate-y-0"
                          leave="transition ease-in duration-150"
                          leaveFrom="opacity-100 translate-y-0"
                          leaveTo="opacity-0 translate-y-1"
                        >
                          <Popover.Panel className='fixed bottom-6 right-8 z-50 w-screen max-w-lg'>
                            {
                              ({ close }) => (
                                <div className="bg-black px-8 pt-4 pb-6 ring ring-black/25">
                                  <div className="mb-4 flex items-center justify-end">
                                    <button className="text-white" onClick={() => close()}>
                                      <IoMdClose size={24} />
                                    </button>
                                  </div>
                                  <div className="mb-4">
                                    <div className="flex items-center bg-primary/80">
                                      <Image
                                        src={`/2022/images//staff-icons/${point.comment.panel?.icon ?? ''}`}
                                        className="aspect-square h-full w-full max-w-sm object-cover object-center transition-transform hover:scale-110"
                                        width={120}
                                        height={120}
                                        alt={point.comment.panel?.name}
                                      />
                                      <div className="flex grow flex-col px-4 font-bold">
                                        <h4 className="text-xl">審査員・講評</h4>
                                        <h5 className="text-sm">{work.title} / {work.author}</h5>
                                        <div className="mt-4 flex items-center gap-1 self-end text-xs">
                                          <BiCommentDots />
                                          {point.comment.panel?.name}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <p className="mb-2 whitespace-pre-line break-words text-justify font-light leading-6">
                                    {point.comment.body}
                                  </p>
                                </div>
                              )
                            }
                          </Popover.Panel>
                        </Transition>
                      </Popover>
                    )
                  }
                </div>
              </td>
            ))}
            <td className={clsx(getCellBgClass(work, "bg-primary"))}>
              <Link
                href={`https://nico.ms/${work.video}`}

                rel="noreferrer"
                passHref
              >
                <a
                  target="_blank"
                  className={clsx(
                    `btn-circle btn`,
                    {
                      "btn-disabled": !work.video
                    }
                  )}

                >
                  <FaPlay />
                </a>
              </Link>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default PcResultTable