import clsx from "clsx"
import Image from "next/image"
import Link from "next/link"
import React, { FC } from "react"

import { Staff } from "@/services/staff"

type Props = {
  staff: Staff
}

const Med1StaffCard: FC<Props> = ({ staff }) => {
  return (
    <div className="relative grid w-full grid-cols-2 gap-2 rounded-lg bg-gray-800 shadow-xl">
      <figure className="aspect-square overflow-hidden bg-black/80">
        <Image
          src={`/2022/images//staff-icons/${staff.icon}`}
          className="aspect-square h-full w-full max-w-sm object-cover object-center transition-transform hover:scale-110"
          width={256}
          height={256}
          alt={staff.name}
        />
      </figure>
      <div className="card-body p-3">
        <h2 className="card-title break-all">{staff.name}</h2>
        <p className="mb-6 text-sm">{staff.role}</p>
        <div className="flex flex-col justify-start gap-1">
          {staff.link.map((link, idx, list) => (
            <Link
              href={link.href}
              key={`staff-link-${idx}`}
              passHref
            >
              {/* eslint-disable-next-line tailwindcss/no-custom-classname */}
              <a className={clsx(`btn-block btn-sm btn rounded uppercase`, `bg-${link.site}`)} target="_blank">
                {link.site}
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Med1StaffCard
