import Head from "next/head"
import React, { FC } from "react"

import MetaData, { MetaDataType } from '@/assets/json/site-metadata.json'

type Props = {
  title?: string
  description?: string
}

const SEO: FC<Props> = ({ title, description }) => {
  const metaData = MetaData as MetaDataType

  const targetTitle = title ? `${metaData.title} | ${title}` : metaData.title
  description ??= metaData.description


  return (
    <Head>
      <title>{targetTitle}</title>

      <meta property='theme-color' content='dark' />
      <meta property='description' content={description} />

      <meta property='og:type' content='website' />
      <meta property='og:title' content={targetTitle} />
      <meta property='og:description' content={description} />

      <meta property='twitter:card' content='summary' />
      <meta property='twitter:creator' content={metaData.author} />
      <meta property='twitter:title' content={targetTitle} />
      <meta property='twitter:description' content={description} />
    </Head>
  )
}

export default SEO
