import clsx from "clsx"
import React, { FC } from "react"

import { Schedule } from "@/assets/json/schedules.json"
import { useDeadline } from "@/utils/useDeadline"

type Props = {
  schedules: Schedule[]
}

const Schedule: FC<Props> = ({ schedules }) => {
  /** 締め切り情報 */
  const { isPast } = useDeadline()

  return (
    <div className="bg-base-100">
      <div className="mx-auto max-w-2xl py-16 text-center">

        <ul className="steps steps-vertical w-full">
          {schedules
            .map((it, idx) => (
              <li
                className={clsx(`step`, {
                  "step-primary": isPast(it.date)
                })}
                key={`step-${idx}`}
              >
                <div
                  className={
                    clsx(`card my-3 w-full`, {
                      "bg-primary text-primary-content": isPast(it.date),
                      "bg-white/90": !isPast(it.date)
                    })
                  }
                >
                  <div className="card-body">
                    <div className="flex items-center justify-between">
                      <h2
                        className={
                          clsx('card-title',
                            {
                              'text-slate-700': !isPast(it.date)
                            }
                          )
                        }
                      >
                        {it.title}
                      </h2>
                      <span className="text-slate-400">{it.date}</span>
                    </div>
                    {it.description && (
                      <p>{it.description}</p>
                    )}
                  </div>
                </div>
              </li>
            ))}
        </ul>
      </div>
    </div>
  )
}

export default Schedule
