import Image from "next/image";
import { FC } from "react";

import { TimeTableWork } from "@/services/schedule";

type Props = {
  timeTable: TimeTableWork[][]
}

const TimeTableList: FC<Props> = ({ timeTable }) => {
  return (
    <div className="grid gap-4 lg:grid-cols-2">
      {
        timeTable.map(((tt, idx) => (
          <div key={`day-${idx + 1}`} className="mb-8 flex w-full flex-col items-center lg:mb-0">
            <h4 className="mb-8 rounded bg-black/30 px-6 py-4 text-center font-sansSerif text-4xl font-bold">{idx + 1}日目</h4>
            <ul className="flex w-full max-w-lg flex-col gap-4">
              {
                tt.map((it) => (
                  <li key={`day-${idx + 1}-${it.entryNo}`} className="card card-side grid grid-cols-3 overflow-hidden rounded border border-white/10 bg-base-100 shadow-xl">
                    <div className="grow overflow-hidden bg-primary/50">
                      <div className="relative h-full w-full">
                        <span className="absolute top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 font-rankPoint text-4xl font-bold">{it.time}</span>
                        <div className="pointer-events-none absolute bottom-0 right-0 z-0 -rotate-12 scale-125 opacity-5">
                          <Image
                            src="/2022/images//logo_white.png"
                            alt="Med1GP 2022"
                            width={98}
                            height={48}
                            loading='eager'
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body col-span-2">
                      <h2 className="card-title">No.{it.entryNo}</h2>
                      <p>{it.author}</p>
                    </div>
                  </li>
                ))
              }
            </ul>
          </div>
        )))
      }
    </div>
  )
}

export default TimeTableList