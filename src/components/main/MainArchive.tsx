import "swiper/css"
import "swiper/css/navigation"
import "swiper/css/scrollbar"

import React, { FC } from "react"
import { A11y, Navigation, Pagination, Scrollbar } from "swiper"
import { Swiper, SwiperSlide } from "swiper/react"

import Med1Heading from "@/components/layouts/Med1Heading"

const pickUpList = [
  "https://ext.nicovideo.jp/thumb_series/320687",
  "https://ext.nicovideo.jp/thumb/sm40425496",
  "https://ext.nicovideo.jp/thumb/sm40394236",
  "https://ext.nicovideo.jp/thumb/sm40392812",
  "https://ext.nicovideo.jp/thumb/sm39867879",
  "https://ext.nicovideo.jp/thumb/sm39901775",
]

const Pickup: FC = () => (
  <div className="bg-black px-8 py-16">
    <Med1Heading en={"Archives"} ja={"アーカイブ"} />
    <Swiper
      modules={[Navigation, Pagination, Scrollbar, A11y]}
      breakpoints={{
        480: { slidesPerView: 1 },
        640: { slidesPerView: 2 },
        960: { slidesPerView: 3 },
        1400: { slidesPerView: 4 },
        1700: { slidesPerView: 5 },
      }}
      navigation
      scrollbar={{ draggable: true }}
    >
      {pickUpList.map((video, idx) => (
        <SwiperSlide key={`pickUp-${idx}`}>
          <div className="flex justify-center">
            <div className="overflow-hidden rounded-lg">
              <iframe
                width="280"
                height="180"
                src={video}
                scrolling="no"
                frameBorder="0"
              ></iframe>
            </div>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  </div>
)

export default Pickup
