import Link from "next/link"
import React from "react"
import { SiMinutemailer, SiNiconico, SiTwitter } from "react-icons/si"
import { TwitterTimelineEmbed } from "react-twitter-embed"

import Med1Heading from "@/components/layouts/Med1Heading"

const SNS = () => (
  <div className="bg-black">
    <div className="mx-auto max-w-2xl px-8 py-16 text-center">
      <div className="mb-10">
        <Med1Heading en={"SNS"} ja={"関連リンク"} />
      </div>
      <div className="flex w-full flex-col lg:flex-row">
        <div className="grid grow">
          <TwitterTimelineEmbed
            sourceType="profile"
            screenName="Med1GP"
            options={{ width: 360, height: 480 }}
          />
        </div>
        <div className="divider lg:divider-horizontal"></div>
        <div className="grow">
          <Link
            href="https://twitter.com/Med1GP"

            rel="noreferrer"
            passHref
          >
            <a
              className="btn-block btn mb-5 gap-2 bg-twitter text-white hover:bg-twitter/80"
              target="_blank"
            >
              <SiTwitter />
              Twitter
            </a>
          </Link>
          <Link
            href="https://com.nicovideo.jp/community/co5633875"

            rel="noreferrer"
            passHref
          >
            <a
              target="_blank"
              className="btn-block btn mb-5 gap-2 bg-niconicommunity text-white hover:bg-niconicommunity/80"
            >
              <SiNiconico />
              ニコニコミュニティ
            </a>
          </Link>
          <Link
            href="https://marshmallow-qa.com/med1gp"

            rel="noreferrer"
            passHref
          >
            <a
              className="btn-block btn mb-5 gap-2 bg-marshmallow text-white hover:bg-marshmallow/80"
              target="_blank"
            >
              <SiMinutemailer />
              マシュマロ
            </a>
          </Link>
        </div>
      </div>
    </div>
  </div>
)

export default SNS
