import Image from "next/image"
import Link from "next/link"
import React, { FC } from "react"
import { BiChevronRight } from "react-icons/bi"

import Med1Heading from "@/components/layouts/Med1Heading"

const Information: FC = () => (
  <div className="hero" style={{ backgroundImage: `url(/2022/images//main-info-bg.webp)` }}>
    <div className="hero-overlay bg-opacity-90" />
    <div className="hero-content text-neutral-content">
      <div className="max-w-2xl p-10 text-center">
        <Med1Heading en={"Information"} ja={"Med-1 GPとは？"} />
        <p className="mb-3">
          約3ヶ月の期間で制作した4分尺のニコニコメドレーを、ニコニコ生放送にて初公開。
        </p>
        <p className="mb-10">
          5人の審査員がぶっつけ本番で採点し結果を発表、優勝作品を1つだけ決定する。
        </p>
        <div className="mb-10">
          <Image
            src="/2022/images//slogan.png"
            alt="目指せ、ニコニコメドレーの頂点。"
            width={480}
            height={42}
            loading='eager'
          />
        </div>
        <div className="mb-4 flex justify-center">
          <div className="btn-group">
            <Link href="/rules" passHref>
              <a className="btn-outline btn-warning btn">
                <span>
                  レギュレーション
                </span>
                <BiChevronRight />
              </a>
            </Link>
            <Link href="/qna" passHref>
              <a className="btn-outline btn-warning btn">
                {"Q&A"}
                <BiChevronRight />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Information
