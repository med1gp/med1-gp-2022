import Image from "next/image"
import Link from "next/link"
import React, { FC, ReactNode } from "react"

import { NormalWork } from "@/services/worklist"
import { MySwal } from "@/utils/Swal"


type ButtonProps = {
  children: ReactNode,
  href: string
  disabled?: boolean
}

const BannerButton: FC<ButtonProps> = ({ children, href, disabled = false }) => {
  const onDisabled = (e: Event) => {
    e.preventDefault()

    MySwal.fire({
      icon: "info",
      text: "生放送の待機所は後日公開予定です。",
    })
  }

  return (
    <Link
      href={href}
      onClick={e => disabled && onDisabled(e)}
      passHref
    >
      <a className="btn-link btn relative p-0 no-underline hover:no-underline" target="_blank">
        <Image src='/2022/images//btn_onair.png' width={184} height={96} />
        <span className="absolute top-3/4 text-xl text-white">{children}</span>
      </a>
    </Link>
  )
}

const Slogan: FC<{ maxHeight?: string | number }> = ({ maxHeight }) => (
  <Image
    src="/2022/images//banner_slogan_after_onair.png"
    width={386}
    height={328}
    objectFit="contain"
    style={{
      maxHeight
    }}
    loading='eager'
  />
)

const WinnerBanner: FC<{ winner: NormalWork }> = ({ winner }) => {
  return (
    <Link
      href={`https://nico.ms/${winner.video}`}

      passHref
    >
      <a className="cursor-pointer transition-transform active:scale-90" target="_blank">
        <Image src="/2022/images//banner_winner.png" objectFit="contain" width={527} height={338} />
      </a>
    </Link>
  )
}

const Trophy = (
  <Image src="/2022/images//trophy.png" objectFit="contain" width={407} height={439} loading='eager' />
)

type Props = {
  winner: NormalWork
}

const Banner: FC<Props> = ({ winner }) => {
  return (
    <div className="relative h-[90vh] overflow-hidden md:h-96">
      {/* パソコン */}
      <div className="hero hidden md:grid">
        <Image
          src="/2022/images//banner_pc.png"
          className="h-full w-full object-cover object-center"
          layout='fill'
          loading='eager'
        />
        <div className="hero-content absolute top-1/2 z-40 grid h-full -translate-y-1/2 grid-flow-col gap-5 p-0">
          <Slogan maxHeight={"60%"} />
          <WinnerBanner winner={winner} />
        </div>
        <div
          className="absolute top-1/2 -translate-y-1/2 p-0"
          style={{
            left: "50%",
            bottom: "-10%",
            maxWidth: "30vw",
          }}
        >
          {Trophy}
        </div>
      </div>

      {/* スマホ */}
      <div className="hero md:hidden">
        <Image
          src="/2022/images//banner_phone.png"
          className="h-[80vh] w-full object-cover object-center"
          layout='fill'
          width={750}
          height={1163}
          loading='eager'
        />
        <div
          className="absolute bottom-[-40%] -translate-y-1/2 p-0"
        >
          {Trophy}
        </div>
        <div
          className="absolute top-1/2 left-1/2 grid w-[80%] -translate-x-1/2 -translate-y-1/2 grid-cols-1 gap-4 p-0"
        >
          <Slogan />
          <WinnerBanner winner={winner} />
        </div>
      </div>
    </div>
  )
}

export default Banner
