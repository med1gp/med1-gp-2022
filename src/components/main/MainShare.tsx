import React, { useEffect, useState } from "react"
import { BiClipboard } from "react-icons/bi"
import { SiHatenabookmark, SiLine, SiTwitter } from "react-icons/si"
import {
  HatenaShareButton,
  LineShareButton,
  TwitterShareButton,
} from "react-share"

import MetaData, { MetaDataType } from '@/assets/json/site-metadata.json'
import { runCopy, runShare } from "@/utils/copyPasteUtils"
import { MySwal } from "@/utils/Swal"

const MainShare = () => {
  /** url共有 */
  const [url, setUrl] = useState("")
  useEffect(() => {
    if (typeof window !== "undefined") {
      setUrl(window.location.href)
    }
  }, [])

  /** コピペ機能 */
  const { title, description } = MetaData as MetaDataType

  const runCopyOrShare = (title: string, description: string, url: string) =>
    !!window?.navigator?.share
      ? runShare(title, description, url)
      : runCopy(title, description, url) &&
      MySwal.fire({
        html: <p className="text-base">サイトの情報がコピーされました。</p>,
        icon: "success",
      })

  return (
    <div className="bg-black">
      <div className="max-w-9xl mx-auto w-full px-8 py-16 md:w-9/12 xl:w-6/12">
        <div className="w-full text-center">
          <div className="mb-8 flex justify-center">
            <iframe
              src="https://embed.nicovideo.jp/watch/sm39867879"
              className="min-h-max w-full"
              width="640"
              height="360"
            />
          </div>
          <div className="mb-8 flex justify-center gap-3">
            <TwitterShareButton
              url={url}
              title={title}
              related={["Med1GP"]}
              hashtags={["Med1GP"]}
            >
              <span className="btn gap-2 bg-twitter text-white hover:bg-twitter/80">
                <SiTwitter />
                <span className="hidden md:inline">ツイートする</span>
              </span>
            </TwitterShareButton>
            <LineShareButton url={url} title={title}>
              <span className="btn gap-2 bg-line text-white hover:bg-line/80">
                <SiLine />
                <span className="hidden md:inline">LINEで共有</span>
              </span>
            </LineShareButton>
            <HatenaShareButton url={url} title={title}>
              <span className="btn bg-hatena hover:bg-hatena/80">
                <SiHatenabookmark />
              </span>
            </HatenaShareButton>
            <button
              className="btn-secondary btn gap-2"
              onClick={() => runCopyOrShare(title, description, url)}
            >
              <BiClipboard />
              共有
            </button>
          </div>
          <hr className="my-8" />
          <div className="mb-8">
            <h5 className="text-2xl ">番組アーカイブ絶賛公開中！</h5>
          </div>
          <div className="flex flex-col justify-center gap-4 md:flex-row">
            <div className="w-full md:w-6/12">
              <h6 className="mb-4">Day 1</h6>
              <iframe
                width="320"
                height="240"
                src="https://embed.nicovideo.jp/watch/sm40453401"
                className="min-h-max w-full"
                loading="lazy"
              ></iframe>
            </div>
            <div className="w-full md:w-6/12">
              <h6 className="mb-4">Day 2</h6>
              <iframe
                width="320"
                height="240"
                src="https://embed.nicovideo.jp/watch/sm40453950"
                className="min-h-max w-full"
                loading="lazy"
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainShare
