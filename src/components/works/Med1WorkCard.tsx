import Image from "next/image"
import Link from "next/link"
import React, { FC } from "react"

import { NormalWork } from "@/services/worklist"


const Thumbnail: FC<Pick<NormalWork, 'video' | 'thumb'>> = ({ video, thumb }) => (
  <div className="relative overflow-hidden">
    <Link
      href={`https://nico.ms/${video}`}
      passHref
    >
      <a target="_blank">
        <Image
          src={`/2022/images//works/${thumb}`}
          width={1280}
          height={720}
          className="aspect-[16/9] h-auto w-full cursor-pointer object-contain transition-transform hover:scale-110"
        />
      </a>
    </Link>
  </div>
)



const Med1WorkCard: FC<NormalWork> = ({
  entryNo,
  title,
  author,
  video,
  thumb,
}) => {
  return (
    <div className="card card-compact w-full border border-primary bg-neutral-900 text-neutral-content shadow-xl">
      <Thumbnail video={video} thumb={thumb} />

      <div className="card-body">
        <div className="flex items-center">
          <div className="mr-3">
            <div className="flex-1 px-2 text-center">
              <h2 className="text-4xl font-bold text-primary">
                {`${entryNo}`.padStart(3, "0")}
              </h2>
              <p className="text-sm text-opacity-80">Entry No</p>
            </div>
          </div>
          <div>
            <p className="card-title">{author}</p>
            <p>「{title}」</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Med1WorkCard
