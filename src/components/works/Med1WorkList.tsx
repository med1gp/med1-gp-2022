import React, { FC, ReactNode } from "react"

import Med1WorkCard from "@/components/works/Med1WorkCard"
import { NormalWork } from "@/services/worklist"

const WorkListWrapper: FC<{ children: ReactNode }> = ({ children }) => (
  <div className="my-10 grid justify-center gap-5 md:grid-cols-2 lg:grid-cols-3 lg:gap-7">
    {children}
  </div>
)

export const WorkListByEntry: FC<{ list: NormalWork[] }> = ({ list }) => (
  <WorkListWrapper>
    {list.map(work => (
      <Med1WorkCard key={`item-${work.entryNo}`} {...work} />
    ))}
  </WorkListWrapper>
)

export const WorkListByNatural: FC<{ first: NormalWork[], second: NormalWork[] }> = ({ first, second }) => (
  <>
    <h5 className="text-5xl font-bold text-white">1日目</h5>
    <WorkListWrapper>
      {first.map(work => (
        <Med1WorkCard key={`item-${work.entryNo}`} {...work} />
      ))}
    </WorkListWrapper>
    <div className="divider" />
    <h5 className="text-5xl font-bold text-white">2日目</h5>
    <WorkListWrapper>
      {second.map(work => (
        <Med1WorkCard key={`item-${work.entryNo}`} {...work} />
      ))}
    </WorkListWrapper>
  </>
)
