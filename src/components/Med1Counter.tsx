import dayjs from "dayjs"
import React, { FC, useEffect, useState } from "react"

const Counter: FC<{ date: string }> = ({ date }) => {
  const [days, setDays] = useState(0)
  const [hour, sethours] = useState(0)
  const [minutes, setMinutes] = useState(0)
  const [seconds, setSeconds] = useState(0)

  const refreshTimer = () => {
    const target = dayjs(`${date} 23:59:59`)
    const now = dayjs()

    const days = target.diff(now, "day")
    const hour = target.diff(now, "hour") % 24
    const minutes = target.diff(now, "minute") % 60
    const seconds = (target.diff(now, "second") % 60) % 60

    setDays(days)
    sethours(hour)
    setMinutes(minutes)
    setSeconds(seconds)

    return window?.setTimeout(refreshTimer, 1000)
  }

  useEffect(() => {
    refreshTimer()
  }, [])

  return (
    <div className="grid auto-cols-max grid-flow-col gap-5 text-center">
      <div className="flex flex-col">
        <span className="countdown font-mono text-5xl">
          {/* @ts-ignore */}
          <span style={{ "--value": `${days}` }}></span>
        </span>
        日
      </div>
      <div className="flex flex-col">
        <span className="countdown font-mono text-5xl">
          {/* @ts-ignore */}
          <span style={{ "--value": `${hour}` }}></span>
        </span>
        時間
      </div>
      <div className="flex flex-col">
        <span className="countdown font-mono text-5xl">
          {/* @ts-ignore */}
          <span style={{ "--value": `${minutes}` }}></span>
        </span>
        分
      </div>
      <div className="flex flex-col">
        <span className="countdown font-mono text-5xl">
          {/* @ts-ignore */}
          <span style={{ "--value": `${seconds}` }}></span>
        </span>
        秒
      </div>
    </div>
  )
}

export default Counter
