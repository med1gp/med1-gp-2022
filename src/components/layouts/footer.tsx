import Image from "next/image"
import React from "react"

function Footer() {
  return (
    <footer className="footer bg-neutral-900 p-10 text-neutral-content">
      <div className="container mx-auto flex justify-center">
        <div className="text-center">
          <div className="mb-3 opacity-20">
            <Image
              src="/2022/images//logo_white.png"
              alt="Med1GP 2022"
              width={98}
              height={48}
              loading='eager'
            />
          </div>
          <p>© 2022 Med-1 グランプリ. All right reserved.</p>
        </div>
      </div>
    </footer>
  )
}

export default Footer
