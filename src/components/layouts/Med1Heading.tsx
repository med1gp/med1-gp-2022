import React, { FC } from "react"

import styles from '@/styles/heading.module.css'

type Props = {
  en: string
  ja: string
}

const Heading: FC<Props> = ({ en, ja }) => (
  <h3 className="mb-4 flex flex-col text-center">
    <span className={styles.en}>{en}</span>
    <span className={styles.ja}>{ja}</span>
  </h3>
)

export default Heading
