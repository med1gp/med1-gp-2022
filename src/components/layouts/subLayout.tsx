import React, { FC,ReactNode } from "react"

import Drawer from "@/components/layouts/drawer"
import styles from '@/styles/sub-heading.module.css'

type Props = {
  children: ReactNode
  pageTitle: {
    ja: string
    en: string
  }
  isContain?: boolean
}

const Layout: FC<Props> = ({ children, pageTitle, isContain = true }) => {
  return (
    <Drawer>
      <div className={styles.banner}>
        <h2 className={styles.ja}>{pageTitle.ja}</h2>
        <p className={styles.en}>{pageTitle.en}</p>
      </div>
      <div className="container mx-auto">
        <main>{children}</main>
      </div>
    </Drawer>
  )
}

export default Layout
