import { FC, ReactNode } from "react"

import Drawer from "@/components/layouts/drawer"

interface Props {
  children: ReactNode;
}

const Layout: FC<Props> = ({ children }) => {
  return (
    <Drawer>
      <main>{children}</main>
    </Drawer>
  )
}

export default Layout
