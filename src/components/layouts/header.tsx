import clsx from "clsx"
import Image from "next/image"
import Link from "next/link"
import React, { FC } from "react"
import { IconContext } from "react-icons"
import { BiMenu } from "react-icons/bi"

type Props = {
  menu: any[]
  onToggle: () => void
}

const Header: FC<Props> = ({ menu, onToggle }) => {
  return (
    <div className="navbar sticky top-0 z-50 bg-neutral-900">
      <div className="container mx-auto flex items-center">
        <div className="grow">
          <Link href="/" passHref>
            <a className="flex items-center text-xl normal-case">
              <Image
                src="/2022/images//logo.png"
                alt="Med1GP 2022"
                width={98}
                height={48}
                loading='eager'
              />
            </a>
          </Link>
        </div>
        <div className="flex-none md:hidden">
          <IconContext.Provider value={{ color: "#fff", size: "2rem" }}>
            <button className="btn-ghost btn-square btn" onClick={onToggle}>
              <BiMenu />
            </button>
          </IconContext.Provider>
        </div>
        <div className="hidden flex-none md:block">
          <ul className="menu menu-horizontal gap-2 p-0">
            {menu.map(({ to, title, accent }, idx) => (
              <li key={`pc-menu-${idx}`} className={clsx({
                'bg-primary/80 rounded font-bold': !!accent
              })}>
                <Link href={to}>{title}</Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Header
