import clsx from "clsx"
import Link from "next/link"
import { useRouter } from "next/router"
import React, { FC, ReactNode, useEffect, useState } from "react"
import { IconContext } from "react-icons"
import { MdClose } from "react-icons/md"

import Footer from "@/components/layouts/footer"
import Header from "@/components/layouts/header"

type Menu = {
  to: string
  title: string
  accent?: boolean
}

const menuList: Menu[] = [
  { to: "/works", title: "参加者・参加作品" },
  { to: "/schedule", title: "タイムテーブル" },
  { to: "/results", title: "結果" },
  { to: "/rules", title: "レギュレーション" },
  { to: "/qna", title: "Q&A" },
  { to: "/staff", title: "スタッフ" },
  { to: "/ed", title: "エンディング", accent: true },
]

const Drawer: FC<{ children: ReactNode }> = ({ children }) => {
  const router = useRouter()
  const [isExpanded, setIsExpanded] = useState(false)

  useEffect(() => {
    setIsExpanded(false)
  }, [router.pathname])

  return (
    <div className="drawer drawer-end z-50 w-full">
      <input
        id="drawer"
        type="checkbox"
        className="drawer-toggle"
        checked={isExpanded}
        onChange={e => setIsExpanded(!!e.target.value)}
      />
      <div className="drawer-content bg-neutral-900" id="drawer-content">
        <Header menu={menuList} onToggle={() => setIsExpanded(val => !val)} />
        <div className="bg-niconico pb-10">
          {children}
        </div>
        <Footer />
      </div>
      <div className="drawer-side">
        <label htmlFor="drawer" className="drawer-overlay" />
        <ul className="menu w-full overflow-y-auto overflow-x-hidden bg-base-100 p-4">
          <li>
            <IconContext.Provider value={{ color: "#fff", size: "1.5rem" }}>
              <a
                className="btn-primary btn gap-2"
                onClick={() => setIsExpanded(false)}
              >
                <MdClose />
                閉じる
              </a>
            </IconContext.Provider>
          </li>
          <li className="divider my-6"></li>
          {menuList.map(({ to, title, accent }, idx) => (
            <li key={`phone-menu-${idx}`} className={clsx({
              'bg-primary/80 rounded font-bold': !!accent
            })}>
              <Link href={to}>{title}</Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Drawer
