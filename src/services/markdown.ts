import * as fs from 'fs'
import path from "path";

export const getMarkdownData = async (filename: string) => {
  const fullPath = path.join(process.cwd(), '/src/assets/docs', `${filename}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');

  return fileContents
}