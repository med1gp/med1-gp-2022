import schedules, { Schedule } from "@/assets/json/schedules.json"

import { getWorkList, NormalWork } from "./worklist"

export const getSchedule: () => Schedule[] = () => {
  return schedules as Schedule[]
}

export type TimeTableWork = NormalWork & {
  time: string
}

export const getTimeTable: () => TimeTableWork[][] = () => {
  const timeList = ['19:45', '20:00', '20:15', '20:30', '20:45', '21:15', '21:30', '21:45', '22:15', '22:30', '22:45']
  const {works1days, works2days} = getWorkList()

  return [
    works1days.map((it, idx) => ({...it, time: timeList[idx]})),
    works2days.map((it, idx) => ({...it, time: timeList[idx]})),
  ]
}