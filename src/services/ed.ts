import edlist from "@/assets/json/edlist.json"

export type MusicTrack = {
  songName: string
  songUrl: string
  specialThanks?: string
}

export type AlbumInfo = {
  albumName?: string
  downloadUrl: string
  songList: MusicTrack[]
}

export const getEdInfo = () => {
  const albumInfo = edlist
  return albumInfo
}