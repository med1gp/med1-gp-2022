import staffInfo from "@/assets/json/staff.json"

type StaffLink = {
  site: string
  href: string
}

export type Staff = {
  name: string
  role: string
  icon: string
  link: StaffLink[]

}

export const getStaffInfo = () => {
  const { host, panel, staff } =  staffInfo

  return {
    host, panel,staff
  }
}