import commentsList from "@/assets/json/comments.json"
import worksList, { Work } from "@/assets/json/works.json"

import { getStaffInfo, Staff } from "./staff"

export const panelList = [
  "タイヤヤ",
  "キットカットおいしい",
  "にしたけ",
  "大内乙打",
  "きっかん",
  "視聴者",
]

export type NormalWork = Work & {

}

export const getWorkList: () => {
  worksList: NormalWork[]
  works1days: NormalWork[]
  works2days: NormalWork[]
} = () => {
  const list = worksList as NormalWork[]

  const works1days = list
    .filter(it => it.day === 1)
    .sort((a, b) => a.order - b.order)

  const works2days = list
    .filter(it => it.day === 2)
    .sort((a, b) => a.order - b.order)

  return {
    worksList: list,
    works1days,
    works2days
  }
}

export type WorkPoint = {
  value: number
  comment: {
    panel: Partial<Staff> | null
    body: string | null
  }
}

export interface ResultWork extends Omit<Work, 'point'> {
  rank: number
  point: WorkPoint[]
  totalPoint: number
}

export const getResultData: () => ResultWork[] = () => {
  const getComment = (entryNo: number) => commentsList.find(it => it.no === entryNo)
  const {panel} = getStaffInfo()

  return (worksList as Work[])
    .map(({point, ...work}) => ({
      ...work,
      point,
      totalPoint: Object.values(point).reduce((a, b) => a + b),
    }))
    .sort((a, b) => b.totalPoint - a.totalPoint)
    .map((it, idx) => {
      const {no, ...commentObj} = getComment(it.entryNo)!

      return ({
        ...it,
        rank: idx + 1,
        point: Object.values(it.point).map((value, pidx) => ({
          value,
          comment: {
            panel: panel.find(it=>it.name === panelList[pidx]) ?? null,
            //  @ts-ignore : It will be get panelN comments
            body: (commentObj[`panel${pidx + 1}`] as string) ?? null
          }
        })
      )
    })
  })
}

export const getWinnerWork: () => NormalWork = () => {
  const winner: NormalWork = ((worksList as Work[]).find(it => it.entryNo === 13)! as NormalWork)

  return winner
  
}