const colors = require("tailwindcss/colors")
const daisyui = require("daisyui")

module.exports = {
  future: {},
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sansSerif: [
          "-apple-system",
          "Noto Sans JP",
          "system-ui",
          "sans-serif",
          "Apple Color Emoji",
          "Segoe UI Emoji",
          "Segoe UI Symbol",
          "Noto Color Emoji",
        ],
        serif: ["Shippori Mincho", "serif"],
        rankPoint: ["Squada One", "Roboto", "sans-serif"],
      },
      colors: {
        ...colors,
        gold: "#b49128",
        silver: "#919391",
        brozen: "#846152",
        niconico: "#252525",
        marshmallow: "#f3969a",
        niconicommunity: "#258d8d",
        hatena: "#009ad9",
        twitter: "#1da1f2",
        line: "#00c300",
      },
    },
  },
  variants: {},
  plugins: [daisyui],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#1D4A96",
          secondary: "#8b5cf6",
          accent: "#37CDBE",
          neutral: "#1c1917",
          "base-100": "#252525",
          info: "#0284c7",
          success: "#10b981",
          warning: "#eab308",
          error: "#ef4444",
        },
      },
    ],
  },
}
