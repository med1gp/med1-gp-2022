module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "next/core-web-vitals",
    "plugin:tailwindcss/recommended",
    "prettier",
  ],
  plugins: ["simple-import-sort", "unused-imports", "tailwindcss"],
  rules: {
    "simple-import-sort/imports": "warn",
    "simple-import-sort/exports": "warn",
    "unused-imports/no-unused-imports": "warn",
    "jsx-a11y/alt-text": "off",
  },
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2015,
  },
}
