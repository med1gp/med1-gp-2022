/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath: '/2022',
  images: {
    unoptimized: true,
  }
}

module.exports = nextConfig
