declare module '@/assets/json/site-metadata.json' {
  type MetaDataType =  {
    "title": string
    "description": string
    "author": string
    "color": string
    "url": string
  }

  export {
    MetaDataType
  }
}