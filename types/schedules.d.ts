declare module '@/assets/json/schedules.json' {
  type Schedule = {
    date: string
    title: string
    description? :string
  }

  type Schedules = Schedule[]

  export {
    Schedule,
    Schedules
  }
}