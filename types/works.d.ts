declare module "@/assets/json/works.json" {
  type PanelPoint = {
    panel1: number
    panel2: number
    panel3: number
    panel4: number
    panel5: number
    panel6: number

  }

  type Work = {
      entryNo: number;
      order: number;
      day: number;
      title: string;
      author: string;
      video: string;
      thumb: string;
      point: PanelPoint
  }

  type WorkList = Work[]

  export {
    Work,
    WorkList
  }
}